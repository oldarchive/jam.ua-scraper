from bs4 import BeautifulSoup
import requests
import csv

def parse(user_link, pages):
    # user_link = input('Paste the link here:\n')
    # pages = int(input('How many pages do you want to scrape?\n'))

    csv_file = open('jam.csv', 'w')
    csv_writer = csv.writer(csv_file)
    csv_writer.writerow(['Название', 'Ссылка', 'Цена'])

    i = 1
    while i<=pages:
        source = requests.get(user_link + f'?list={i}').text
        print(user_link + f'?list={i}')
        soup = BeautifulSoup(source, 'lxml')

        for item in soup.find_all('div', id='catalog_item'):
            name = item.div.text
            link = item.a['href']
            price = item.a.find('div', id='catalog_item_content').find('div', id='catalog_item_price').text.strip()

            csv_writer.writerow([name, link, price])
            print(name, link, price)
        i+=1


    csv_file.close()
