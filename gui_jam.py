import tkinter as tk
import jam

window = tk.Tk()

window.title("Jam Scraper")

link_label = tk.Label(text='Paste the link here:')
link_label.pack()

link_entry = tk.Entry(width = 50)
link_entry.pack()

page_label = tk.Label(text='Paste the number of pages to scrape here:')
page_label.pack()

page_entry = tk.Entry(width = 50)
page_entry.pack()

def handle_click():
    u_link = link_entry.get()
    p = int(page_entry.get())
    jam.parse(u_link, p)

button = tk.Button(text='Scrape', command=handle_click)
button.pack()

window.mainloop()
